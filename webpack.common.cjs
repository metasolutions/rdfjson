module.exports = {
  mode: 'production',
  entry: './src/main.js',
  resolve: {
    fallback: {
      fs: false
    },
    alias: {
      md5: 'md5/md5',
    },
  },
};
