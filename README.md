# RDFJSON library
The RDFJSON library provides an RDF API with support for manipulating RDF graphs and individual statements.
Relies on the RDF/JSON representation internally. Provides both utility functionality for manipulating
the RDF/JSON expression directly and an object-oriented layer on top of it for simplified interactions.

## Getting started

The library is available from npm, i.e.:

    npm install @entryscape/rdfjson

or

    yarn add @entryscape/rdfjson

The library works with either commonjs or es-modules environment, i.e.:

    const { Graph } = require("@entryscape/rdfjson");

or

    import { Graph } from "@entryscape/rdfjson");

In a browser setting you can either work in an ES module settings and point to the files you need in `src` or you can load the prebuilt version in `dist/rdfjson.js`. In the latter case you will have access to a global called `rdfjson`.

The main entrypoints of the library is `Graph.js`, `namespaces.js` and `converters.js`. In addition the following modules are also provided in the default export:

- `Statement.js`
- `Rdfs.js`
- `Utils.js`
- `print.js`

## Using the library
To create a simple graph and add statements you do the following (assuming running in a browser setting with the global `rdfjson`):

    var g = new rdfjson.Graph({});
    g.addL("http://example.com", "dcterms:title", "A title");
    g.add("http://example.com", "dcterms:relation", "http://example.org");

Now, to query the graph we use the find method which can have null in one, two or three places:

    var stmts = g.find(null, "dcterms:relation", null);
    console.log("Found a relation from " + stmts[0].getSubject() + " to " + stmts[0].getValue());

Please check the documentation in ```Graph.js``` and ```Statement.js``` or for that matter look at the tests.
You can also take a look at the samples in examples folder. 

## Development
To get the library you can clone it from [bitbucket](https://bitbucket.org/metasolutions/rdfjson), e.g.:

    git clone git@bitbucket.org:metasolutions/rdfjson.git

Second, to allow the tests to run and build the library you need to have [nodejs](http://nodejs.org/), [npm](https://www.npmjs.org/) and [yarn](https://yarnpkg.com/) installed.

Get the dependencies by running `yarn`.

To build the project you run:

    yarn build:all

To make sure everything works as expected you can run the tests:

    yarn tests

## Contributions
The library is at this point open source but not open contribution. Please get in contact with us at [entryScape.com](https://entryscape.com/en/contact-us/) if you want to help out or have suggestions.

Issues are managed [in a public Jira project](https://metasolutions.atlassian.net/jira/software/c/projects/RDFJSON/issues/).
