import Graph from '../src/Graph.js';
import rdf1 from './files/rdf1.js';
import assert from 'assert';

describe('Projection', () => {
  it('Single value projection', () => {
    const g1 = new Graph(JSON.parse(JSON.stringify(rdf1.graph)));
    const proj = g1.projection('http://example.org/about', {
      creator: 'dcterms:creator',
    });
    assert.ok(proj.creator === 'Anna Wilder', 'Could not project a value');
  });
  it('2-level projection', () => {
    const g1 = new Graph(JSON.parse(JSON.stringify(rdf1.graph)));
    const proj = g1.projection('http://example.org/about', {
      first: ['foaf:maker', 'foaf:firstName']
    })
    assert.ok(proj.first === 'Anna', 'Could not project path');
  });
  it('Projection array', () => {
    const g1 = new Graph(JSON.parse(JSON.stringify(rdf1.graph)));
    const proj = g1.projection('http://example.org/about', {
      '*nick': ['foaf:maker', 'foaf:nick']
    })
    assert.ok(proj.nick.length === 2, 'Could not force array for repeated values');
  });
});
