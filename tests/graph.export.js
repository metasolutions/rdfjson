import Graph from '../src/Graph.js';
import assert from 'assert';

const subject = 'http://example.com';
const predicate = 'http://example.com/predicate';

describe('Graph Create', () => {
  it('should create a graph with duplicate statements which are removed on export', () => {
    const g = new Graph();
    g.add(subject, predicate, 1, false, true);
    g.add(subject, predicate, 1, false, true);
    const g2 = new Graph(g.exportRDFJSON());
    assert.ok(g.find().length === 2);
    assert.ok(g2.size() === 1);
  });
});
