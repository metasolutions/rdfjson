import converters from '../src/converters.js';
import rdf1 from './files/rdf1.js';
import assert from 'assert';

const { rdfxml2graph, detect } = converters;

describe('Converters', () => {
  it('rdfxml2graph', async () => {
    const g = await rdfxml2graph(rdf1.rdfxml);
    assert.strictEqual(g.find().length, 14);
  });

  it('rdfxml2graph inline namespace', async () => {
    const g = await rdfxml2graph(rdf1.rdfxml2);
    assert.strictEqual(g.find().length, 1);
    assert.strictEqual(g.find()[0].getLanguage(), 'en');
  });

  it('rdfxml2graph CDATA', async () => {
    const g = await rdfxml2graph(rdf1.rdfxml3);
    assert.strictEqual(g.find().length, 2);
    assert.strictEqual(g.find(null, 'dcterms:title')[0].getValue(), 'Titel');
  });

  it('detect', async () => {
    const report = await detect(rdf1.rdfxml);
    assert.strictEqual(report.format, 'rdf/xml', `Expected format rdf/xml, got "${report.format}" instead.`);
    assert.strictEqual(report.error, undefined);
  });

  it('detectSpaceError', async () => {
    const report = await detect(rdf1.rdfxmlSpace);
    assert.strictEqual(report.errorCode, 1, 'Expected blank character in URI to be noticed during parsing.');
  });

  it('detectIllegalCharacter1', async () => {
    const report = await detect(rdf1.rdfxmlIllegal1);
    assert.strictEqual(report.errorCode, 1, 'Expected illegal character in URI to be noticed during parsing.');
  });

  it('detectMalformed', async () => {
    const report = await detect(rdf1.rdfxmlMalformed);
    assert.strictEqual(report.errorCode, 2, 'Expected parsing error since closing tag is wrong.');
  });

  it('detectMalformed2', async () => {
    const report = await detect(rdf1.rdfxmlMalformed2, 'Expected parsing error since rdf:about used wrong.');
    assert.strictEqual(report.errorCode, 2);
  });

  it('quadsExport', async () => {
    const g = await rdfxml2graph(rdf1.rdfxml);
    assert.strictEqual(g.toQuads().length, 14);
  });
});
