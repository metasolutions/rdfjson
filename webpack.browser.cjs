const { merge } = require('webpack-merge');
const path = require('path');
const config = require('./webpack.common.cjs');

module.exports = merge(config, {
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'rdfjson.js',
    library: 'rdfjson',
    libraryTarget: 'window',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [['@babel/preset-env', {
              shippedProposals: true,
              useBuiltIns: 'usage',
              corejs: 3,
              targets: { ie: 11 },
            }]],
          }
        },
      },
    ],
  },
});
