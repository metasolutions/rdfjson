const path = require('path');
const { merge } = require('webpack-merge');
const config = require('./webpack.browser.cjs');

module.exports = merge(config, {
  mode: 'development',
  devServer: {
    hot: true,
    contentBase: path.join(__dirname, 'examples'),
    watchOptions: {
      ignored: /node_modules/,
    },
  },
});
