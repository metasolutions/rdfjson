const { merge } = require('webpack-merge');
const path = require('path');
const config = require('./webpack.common.cjs');

module.exports = merge(config, {
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'rdfjson.node.cjs',
    libraryTarget: 'commonjs2',
  },
  devtool: 'cheap-module-source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [['@babel/preset-env', {
              shippedProposals: true,
              useBuiltIns: 'usage',
              corejs: 3,
              targets: {
                "node": "current"
              }
            }]],
          }
        },
      },
    ],
  },
});
