import dataFactory from '@rdfjs/data-model';

const fromBlank = (value) => {
  if (value.indexOf('_:') === 0) {
    return value.substring(2);
  }
  return value;
}
const fromObject = (object) => {
  if(!object.value) return undefined;
  switch (object.type) {
    case 'literal':
      if (object.lang) return dataFactory.literal(object.value, object.lang);
      if (object.datatype) {
        const datatype = dataFactory.namedNode(object.datatype);
        return dataFactory.literal(object.value, datatype);
      }
      return dataFactory.literal(object.value);
    case 'uri':
      return dataFactory.namedNode(object.value);
    case 'bnode':
      return dataFactory.blankNode(fromBlank(object.value));
  }
};

/**
 * Converts a graph to an array of quads
 * @returns {Array}
 * @private
 */
export const toQuads = (graph) => graph.find().map((statement) => {
  const subject = statement.getSubject();
  const predicate = statement.getPredicate();
  const object = fromObject(statement.getObject());
  const ng = statement.getNamedGraph() ? dataFactory.namedNode(statement.getNamedGraph()) : undefined;
  return dataFactory.quad(
    statement.isSubjectBlank() ? dataFactory.blankNode(fromBlank(subject)) : dataFactory.namedNode(subject),
    dataFactory.namedNode(predicate), // TODO check if predicate is blank or not
    object,
    ng
  );
});

const toBlank  = (value) => {
  if (value.indexOf('_:') === 0) {
    return value;
  }
  return `_:${value}`;
}

/**
 * Adds
 * @param quads
 * @param graph
 * @private
 */
export const toGraph = (quads, graph) => {
  const _graph = graph || new Graph();
  const extractObject = (term) => {
    switch (term.termType) {
      case 'NamedNode':
        return { value: term.value, type: 'uri' };
      case 'BlankNode':
        return { value: toBlank(term.value), type: 'bnode' };
      case 'Literal':
        if (term.language) {
          return { value: term.value, type: 'literal', lang: term.language };
        } else if (term.datatype) {
          return { value: term.value, type: 'literal', datatype: term.datatype.value };
        }
        return { value: term.value, type: 'literal' };
      case 'Variable':
      case 'DefaultGraph':
    }
  };

  const extractObjectAndNG = (quad) => {
    const obj = extractObject(quad.object || quad._object);
    if (quad.graph.termType !== 'DefaultGraph') {
      obj.ng = quad.graph.value;
    }
    return obj;
  };

  quads.forEach(quad => {
    _graph.add(
      quad.subject.termType === 'BlankNode' ? toBlank(quad.subject.value) : quad.subject.value,
      quad.predicate.termType === 'BlankNode' ? toBlank(quad.predicate.value) : quad.predicate.value,
      extractObjectAndNG(quad)
    );
  })
};