import { RdfXmlParser } from "rdfxml-streaming-parser";
import dataFactory from '@rdfjs/data-model';
import Graph from '../../Graph.js';

class CustomError extends Error {
  constructor(code = '1', ...params) {
    super(...params)
    this.code = code
  }
}

const rdfXmlVocab = (uri) => {
  if (uri.startsWith('http://www.w3.org/1999/02/22-rdf-syntax-ns#')) {
    const localname = uri.substring(43);
    const names = ['about', 'nodeID', 'resource', 'parseType', 'datatype', 'li', 'ID', 'Description', 'RDF'];
    return names.includes(localname);
  }
  return false;
}

const xmlVocab = uri => uri === 'http://www.w3.org/2000/xmlns/xmlns';

export default async (xml, graph = null) => new Promise((success, reject) => {
  /**
   * @type {rdfjson.Graph}
   */
  const g = graph instanceof Graph ? graph : new Graph();
  const quads = [];
  const myParser = new RdfXmlParser({
    dataFactory: dataFactory,
    strict: true
  });
  let errors = false;
  myParser
    .on('data', (q) => {
      if (!errors) {
        if (q.subject.termType === 'NamedNode' && rdfXmlVocab(q.subject.value)) {
          reject(new CustomError(2, `RDF/XML Syntax error, ${q.subject.value} in subject position`));
          errors = true;
          return;
        }
        if (q.predicate.termType === 'NamedNode') {
          if (rdfXmlVocab(q.predicate.value)) {
            reject(new CustomError(2, `RDF/XML Syntax error, ${q.subject.value} in predicate position`));
            errors = true;
            return;
          } else if (xmlVocab(q.predicate.value)) {
            return;
          }
        }
        if (q.object.termType === 'NamedNode' && rdfXmlVocab(q.object.value)) {
          reject(new CustomError(2, `RDF/XML Syntax error, ${q.subject.value} in object position`));
          errors = true;
          return;
        }
        quads.push(q);
      }
    })
    .on('error', (err) => {
      if (!errors) {
        errors = true
        if (err.message.includes('Invalid IRI')) {
          reject(new CustomError(1, err.message));
        } else {
          reject(new CustomError(2, err.message));
        }
      }
    })
    .on('end', () => {
      if (!errors) {
        g.addQuads(quads);
        success(g);
      }
    });
  myParser.write(xml);
  myParser.end();
});