import ns from '../../namespaces.js';
import Graph from '../../Graph.js';

const sp = '  ';
const sp2 = '    ';
const xmlEncode = url => encodeURI(decodeURI(url)).replace(/&/g, '&amp;');

const toRDFXML = (graph) => {
  const g = graph instanceof Graph ? graph._graph : graph || {};
  const nsUsed = [];
  const nsAdded = {};
  const nsify = function (prop) {
    const _o = ns.nsify(prop);
    if (!nsAdded[_o.abbrev]) {
      nsUsed.push(_o.abbrev);
      nsAdded[_o.abbrev] = _o.ns;
    }
    return _o.pretty;
  };

  const strs = [];
  Object.keys(g).forEach((s) => {
    if (s.substring(0, 2) === '_:') {
      strs.push(`${sp}<rdf:Description rdf:nodeID="_${s.substring(2)}">\n`);
    } else {
      strs.push(`${sp}<rdf:Description rdf:about="${xmlEncode(s)}">\n`);
    }
    const props = g[s];
    Object.keys(props).forEach((p) => {
      const nsp = nsify(p);
      props[p].forEach((o) => {
        let v;
        switch (o.type) {
          case 'literal':
            v = o.value.replace(/&/g, '&amp;').replace(/</g, '&lt;');
            if (o.lang != null) {
              strs.push(`${sp2}<${nsp} xml:lang="${o.lang}">${v}</${nsp}>\n`);
            } else if (o.datatype != null) {
              strs.push(`${sp2}<${nsp} rdf:datatype="${o.datatype}">${v}</${nsp}>\n`);
            } else {
              strs.push(`${sp2}<${nsp}>${v}</${nsp}>\n`);
            }
            break;
          case 'uri':
            strs.push(`${sp2}<${nsp} rdf:resource="${xmlEncode(o.value)}"/>\n`);
            break;
          case 'bnode':
            if (o.value.substr(0, 2) === '_:') {
              strs.push(`${sp2}<${nsp} rdf:nodeID="_${o.value.substring(2)}"/>\n`);
            } else {
              strs.push(`${sp2}<${nsp} rdf:nodeID="${o.value}"/>\n`);
            }
            break;
          default:
        }
      });
    });
    strs.push(`${sp}</rdf:Description>\n`);
  });
  const initialStrs = ['<?xml version="1.0"?>\n<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"'];
  for (let j = 0; j < nsUsed.length; j++) {
    if (nsUsed[j] !== 'rdf') {
      initialStrs.push(`\n\txmlns:${nsUsed[j]}="${nsAdded[nsUsed[j]]}"`);
    }
  }
  initialStrs.push('>\n');
  strs.unshift(initialStrs.join(''));
  strs.push('</rdf:RDF>');
  return strs.join('');
};

export default toRDFXML;