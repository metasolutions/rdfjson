import Graph from './Graph.js';
import toRDFXML from "./formats/rdfxml/toRDFXML.js";
import fromRDFXML from "./formats/rdfxml/fromRDFXML.js";

/**
 *
 * @module rdfjson/converters
 */

/**
 * Detects RDF from a string expressed in the RDF/XML or RDF/JSON formats. In addition,
 * it also understands instances of Graph or objects following the RDF/JSON structure.
 *
 * @param {string|object} rdf RDF graph in some format.
 * @returns {{graph: Graph, format: string, error: string, errorCode: number}} a report containing the detected graph
 * and format. If the detection fails the report contains a clear text error and an error code.
 */
const detect = async (rdf) => {
  const report = {};
  if (typeof rdf === 'string') {
    const taste = rdf.substring(0, 200);
    if (taste.toLowerCase().indexOf('<rdf:rdf') !== -1) {
      report.format = 'rdf/xml';
      try {
        report.graph = await fromRDFXML(rdf);
      } catch (e) {
        report.error = e.message;
        report.errorCode = e.code;
      }
    } else if (rdf.substring(0, 2) === '{"') {
      report.format = 'rdf/json';
      try {
        const jsonrdf = JSON.parse(this.rdfjson);
        report.graph = new Graph(jsonrdf);
      } catch (e) {
        report.error = 'Invalid json.';
        report.errorCode = 3;
      }
    } else {
      report.error = 'No RDF detected.';
      report.errorCode = 4;
    }
  } else if (rdf instanceof Graph) {
    report.format = 'rdf/json';
    report.graph = rdf;
  } else if (typeof rdf === 'object' && rdf !== null) {
    report.format = 'rdf/json';
    report.graph = new Graph(rdf);
  } else {
    report.error = 'unknown format';
    report.errorCode = 5;
  }
  if (!report.error) {
    const r = report.graph.validate();
    if (!r.valid) {
      report.error = 'RDF/JSON is not valid.';
      report.errorCode = 6;
    }
  }
  return report;
};

/**
 * Imports RDF/XML into a Graph
 *
 * @param {String} xml a string in the RDF/XML format.
 * @param {Graph|undefined} graph triples will be added to this graph, if undefined a new graph
 * will be created.
 * @returns {Graph} graph containing all the triples of the RDF/XML.
 * @function
 */
const rdfjson2rdfxml = toRDFXML;

/**
 * Imports RDF/XML into a Graph
 *
 * @param {String} xml a string in the RDF/XML format.
 * @param {Graph|undefined} graph triples will be added to this graph, if undefined a new graph
 * will be created.
 * @returns {Graph} graph containing all the triples of the RDF/XML.
 * @function
 */
const rdfxml2graph = fromRDFXML;

export default {
  rdfxml2graph,
  rdfjson2rdfxml,
  detect,
};
